/*
   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "freertos/xtensa_api.h"
#include "freertos/FreeRTOSConfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "bt_app_core.h"
#include <esp_avrc_api.h>
#include <esp_a2dp_api.h>
#include "audio_renderer.h"
#include "esp_sleep.h"
#include "esp_bt_main.h"
#include "soc/timer_group_struct.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"
#include "driver/rtc_io.h"
#include "audio_example_file.h"
#include "driver/i2s.h"
#include "esp_partition.h"
#include "esp_spi_flash.h"


#define EXAMPLE_I2S_NUM (0)
#define EXAMPLE_I2S_SAMPLE_RATE (16000)
#define EXAMPLE_I2S_READ_LEN (16 * 1024)

esp_a2d_audio_state_t estado = ESP_A2D_AUDIO_STATE_STOPPED;
static void bt_app_task_handler(void *arg);
static bool bt_app_send_msg(bt_app_msg_t *msg);
static void bt_app_work_dispatched(bt_app_msg_t *msg);

static xQueueHandle bt_app_task_queue = NULL;
static xTaskHandle bt_app_task_handle = NULL;
static xQueueHandle gpio_evt_queue = NULL;

int contador = 0;

typedef struct {
    int type;  // the type of timer's event
    int timer_group;
    int timer_idx;
    uint64_t timer_counter_value;
} timer_event_t;

static void inline print_timer_counter(uint64_t counter_value)
{
    printf("Counter: 0x%08x%08x\n", (uint32_t) (counter_value >> 32),
                                    (uint32_t) (counter_value));
    printf("Time   : %.8f s\n", (double) counter_value / TIMER_SCALE);
}


bool bt_app_work_dispatch(bt_app_cb_t p_cback, uint16_t event, void *p_params, int param_len, bt_app_copy_cb_t p_copy_cback)
{
    ESP_LOGD(BT_APP_CORE_TAG, "%s event 0x%x, param len %d", __func__, event, param_len);

    bt_app_msg_t msg;
    memset(&msg, 0, sizeof(bt_app_msg_t));

    msg.sig = BT_APP_SIG_WORK_DISPATCH;
    msg.event = event;
    msg.cb = p_cback;

    if (param_len == 0) {
        return bt_app_send_msg(&msg);
    } else if (p_params && param_len > 0) {
        if ((msg.param = malloc(param_len)) != NULL) {
            memcpy(msg.param, p_params, param_len);
            /* check if caller has provided a copy callback to do the deep copy */
            if (p_copy_cback) {
                p_copy_cback(&msg, msg.param, p_params);
            }
            return bt_app_send_msg(&msg);
        }
    }

    return false;
}

static bool bt_app_send_msg(bt_app_msg_t *msg)
{
    if (msg == NULL) {
        return false;
    }

    if (xQueueSend(bt_app_task_queue, msg, 10 / portTICK_RATE_MS) != pdTRUE) {
        ESP_LOGE(BT_APP_CORE_TAG, "%s xQueue send failed", __func__);
        return false;
    }
    return true;
}

static void bt_app_work_dispatched(bt_app_msg_t *msg)
{
    if (msg->cb) {
        msg->cb(msg->event, msg->param);
    }
}

static void bt_app_task_handler(void *arg)
{
    bt_app_msg_t msg;
    for (;;) {
        if (pdTRUE == xQueueReceive(bt_app_task_queue, &msg, (portTickType)portMAX_DELAY)) {
            ESP_LOGD(BT_APP_CORE_TAG, "%s, sig 0x%x, 0x%x", __func__, msg.sig, msg.event);
            switch (msg.sig) {
            case BT_APP_SIG_WORK_DISPATCH:
                bt_app_work_dispatched(&msg);
                break;
            default:
                ESP_LOGW(BT_APP_CORE_TAG, "%s, unhandled sig: %d", __func__, msg.sig);
                break;
            } // switch (msg.sig)

            if (msg.param) {
                free(msg.param);
            }
        }
    }
}



int example_i2s_dac_data_scale(uint8_t* d_buff, uint8_t* s_buff, uint32_t len)
{
    uint32_t j = 0;
#if (EXAMPLE_I2S_SAMPLE_BITS == 16)
    for (int i = 0; i < len; i++) {
        d_buff[j++] = 0;
        d_buff[j++] = s_buff[i];
    }
    return (len * 2);
#else
    for (int i = 0; i < len; i++) {
        d_buff[j++] = 0;
        d_buff[j++] = 0;
        d_buff[j++] = 0;
        d_buff[j++] = s_buff[i];
    }
    return (len * 4);
#endif
}

void IRAM_ATTR timer_group0_isr(void *para){
    int timer_idx = (int) para;
     uint32_t intr_status = TIMERG0.int_st_timers.val;
     // EL IF ES NECESARIO PARA LIMPIAR LA INTERRUPCION
     // Y REINICIAR EÑ COUNTER
      if((intr_status & BIT(timer_idx)) && timer_idx == TIMER_0) {
          TIMERG0.hw_timer[timer_idx].update = 1;
          TIMERG0.int_clr_timers.t0 = 1;
          TIMERG0.hw_timer[timer_idx].config.alarm_en = 1;
          esp_deep_sleep_start();
          
      }
}


void example_set_file_play_mode()
{
    i2s_set_clk(EXAMPLE_I2S_NUM, 16000, EXAMPLE_I2S_SAMPLE_BITS, 1);
}


void isr_button_on_off(void *args)
{
	//esp_deep_sleep_start();
	xQueueSendFromISR(gpio_evt_queue, &contador, NULL);
	contador++;
	
}


//void isr_button_on(void *args){
	
	//esp_deep_sleep_start();
	
//}
	


void isr_button_pressed(void *args)
{
	if(ESP_AVRC_CT_CONNECTION_STATE_EVT == 0){	
		int btn_state = gpio_get_level(BUTTON_GPIO_1);	
		switch(estado){
			case ESP_A2D_AUDIO_STATE_STARTED:
				esp_avrc_ct_send_passthrough_cmd(1, ESP_AVRC_PT_CMD_STOP,ESP_AVRC_PT_CMD_STATE_RELEASED);	
				estado = ESP_A2D_AUDIO_STATE_STOPPED;
				gpio_set_level(LED_GPIO,btn_state);
				break;
				
			case ESP_A2D_AUDIO_STATE_STOPPED:
						esp_avrc_ct_send_passthrough_cmd(1, ESP_AVRC_PT_CMD_PLAY,ESP_AVRC_PT_CMD_STATE_RELEASED);
						estado = ESP_A2D_AUDIO_STATE_STARTED;
						gpio_set_level(LED_GPIO,btn_state);
						break;
			case ESP_A2D_AUDIO_STATE_REMOTE_SUSPEND:
						esp_avrc_ct_send_passthrough_cmd(1, ESP_AVRC_PT_CMD_PLAY,ESP_AVRC_PT_CMD_STATE_RELEASED);
						estado = ESP_A2D_AUDIO_STATE_STARTED;	
						gpio_set_level(LED_GPIO,btn_state);
						break;				
			
		}
	}	
}

void timer0_init()
{
	
    /* Select and initialize basic parameters of the timer */
    int timer_group = TIMER_GROUP_0;
    int timer_idx = TIMER_0;
    timer_config_t config;
    config.alarm_en = 1;
    config.auto_reload = 1;
    config.counter_dir = TIMER_COUNT_UP;
    config.divider = TIMER_DIVIDER;
    config.intr_type = TIMER_INTR_LEVEL;
    config.counter_en = TIMER_PAUSE;
    
    /*Configure timer*/
    timer_init(timer_group, timer_idx, &config);
    /*Stop timer counter*/
    timer_pause(timer_group, timer_idx);
    /*Load counter value */
    timer_set_counter_value(timer_group, timer_idx, 0x00000000ULL);
    /*Set alarm value*/
    timer_set_alarm_value(timer_group, timer_idx, (TIMER_INTERVAL0_SEC * TIMER_SCALE) - TIMER_FINE_ADJ);
    /*Enable timer interrupt*/
    timer_enable_intr(timer_group, timer_idx);
    /*Set ISR handler*/
    timer_isr_register(timer_group, timer_idx, timer_group0_isr, (void*) timer_idx, ESP_INTR_FLAG_IRAM, NULL);

}

void timer0_task_off(void *PvParameter)
{    
	
    while(1){
		double time_juan;
		timer_get_counter_time_sec(TIMER_GROUP_0, TIMER_0, &time_juan);
		ESP_LOGI("test", "Task Timer Initialized: Time %lg\n", time_juan);		
		vTaskDelay(5000/portTICK_PERIOD_MS);
		
	}
}

void init_sound (void *pvParameter)
{
	
	printf("Playing file example: \n");
	int offset = 0;
	int tot_size = sizeof(audio_table);
	example_set_file_play_mode();
	while (offset < tot_size) {
		int play_len = ((tot_size - offset) > (4 * 1024)) ? (4 * 1024) : (tot_size - offset);
		int i2s_wr_len = example_i2s_dac_data_scale(i2s_write_buff, (uint8_t*)(audio_table + offset), play_len);
		i2s_write(EXAMPLE_I2S_NUM, i2s_write_buff, i2s_wr_len, &bytes_written, portMAX_DELAY);
		offset += play_len;
		//example_disp_buf((uint8_t*) i2s_write_buff, 32);
	}
	vTaskDelay(100 / portTICK_PERIOD_MS);
	//example_reset_play_mode();
    free(flash_read_buff);
    free(i2s_write_buff);
	vTaskDelete(NULL);
}
	


void task_button_to_led(void *pvParameter)
{
	  //Configure button
	  gpio_config_t btn_config;
	  btn_config.intr_type = GPIO_INTR_POSEDGE; 	//Enable interrupt on both rising and falling edges
	  btn_config.mode = GPIO_MODE_INPUT;        	//Set as Input
	  btn_config.pin_bit_mask = BUTTONS_GPIO; //Bitmask
	  btn_config.pull_up_en = GPIO_PULLUP_DISABLE; 	//Disable pullup
	  btn_config.pull_down_en = GPIO_PULLDOWN_ENABLE; //Enable pulldown
	  gpio_config(&btn_config);

	  //Configure LED
	  gpio_pad_select_gpio(LED_GPIO);					//Set pin as GPIO
	  gpio_set_direction(LED_GPIO, GPIO_MODE_OUTPUT);	//Set as Output
	  //printf("LED configured\n");
		
	  //Configure interrupt and add handler
	  gpio_install_isr_service(0);						//Start Interrupt Service Routine service
	  gpio_isr_handler_add(BUTTON_GPIO_1, isr_button_pressed, NULL); //Add handler of interrupt
	  gpio_isr_handler_add(BUTTON_GPIO_2, isr_button_on_off, NULL); //Add handler of interrupt
	  //gpio_isr_handler_add(BUTTON_GPIO_3, isr_button_on, NULL); //Add handler of interrupt
	  
	  rtc_gpio_pulldown_en(BUTTON_GPIO_3);
	  //esp_sleep_enable_ext0_wakeup(BUTTON_GPIO_3, 1);
	  
	  //const int ext_wakeup_pin_1 = BUTTON_GPIO_3;
	  //const uint64_t ext_wakeup_pin_1_mask = 1ULL << ext_wakeup_pin_1;
	  //esp_sleep_enable_ext1_wakeup(ext_wakeup_pin_1_mask, ESP_EXT1_WAKEUP_ANY_HIGH);
	  

	   
	 
	  
	  //esp_sleep_enable_ext0_wakeup(BUTTON_GPIO_3, 1);


	  //Wait
	  while (1)
	  {
		  int cont;
		  if(xQueueReceive(gpio_evt_queue, &cont, portMAX_DELAY)) {
            printf("Pulsado %d veces el boton off\n", cont);
          }
		  //ESP_LOGI("test", "Waiting for button press....");
		  vTaskDelay(3000/portTICK_PERIOD_MS);
	  }
	 
}




void bt_app_task_start_up(void)
{
    bt_app_task_queue = xQueueCreate(10, sizeof(bt_app_msg_t));
    xTaskCreate(bt_app_task_handler, "BtAppT", 2048, NULL, configMAX_PRIORITIES - 3, bt_app_task_handle);
    return;
}

void init_sound_task() 
{
	
	xTaskCreate(&init_sound, "timer_task_off", 2048, NULL, 5, NULL);	
}

void bt_int_stop_play()
{
	gpio_evt_queue = xQueueCreate(10, sizeof(int));
	//timer_queue = xQueueCreate(10, sizeof(status_esp));
	xTaskCreate(&task_button_to_led, "buttonToLED", 2048, NULL, 5, NULL);
}

void bt_timer_off()
{
	xTaskCreate(&timer0_task_off, "timer_task_off", 2048, NULL, 5, NULL);
	timer0_init();
	
}


void bt_app_task_shut_down(void)
{
    if (bt_app_task_handle) {
        vTaskDelete(bt_app_task_handle);
        bt_app_task_handle = NULL;
    }
    if (bt_app_task_queue) {
        vQueueDelete(bt_app_task_queue);
        bt_app_task_queue = NULL;
    }
}
